from main import bot, dp

from aiogram.types import Message
from config import admin_id
from trecker import get_feed
from cat import get_forums
forums = {}
forums = get_forums(forums)
nl = '\n'

async def send_to_admin(dp):
    await bot.send_message(chat_id=admin_id, disable_web_page_preview=True, text='RuTop is Running')


@dp.message_handler(commands='new')
async def echo(message: Message):
    topics = get_feed()
    my_tops = list(topics.values())
    my_tops = sorted(my_tops, key=lambda i: int(i['down']))

    for topic in my_tops:
        print(topic)
        link = f'<a href="{topic["link"]}">{topic["just_title"]}</a>'
        await bot.send_message(chat_id=message.from_user.id, disable_web_page_preview=True, parse_mode='HTML',
                               text=f'{forums[topic["category"]]["main"][-1]}'
                                    f'{nl}{forums[topic["category"]]["forum"][-1]}'
                                    f'{nl}{forums[topic["category"]]["category"][-1]}'
                                    f'{nl}{topic["id"]}, {topic["down"]}'
                                    f'{nl}{link}, ')


