import asyncio
from aiogram import Bot, Dispatcher, executor

from config import BOT_TOKEN
from trecker import get_feed


if __name__ == "__main__":
    from handler2s import dp, send_to_admin
    executor.start_polling(dp, on_startup=send_to_admin)


