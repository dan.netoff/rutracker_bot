from pprint import pprint

from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

from config import URL_APPLES, URL_PEAR
from keyboards.inline.callback_datas import buy_callback

from cat import get_forums

forums = {}
forums = get_forums(forums)


def myf(item):
    if item['forum'][0] == '22':
        return True
    else:
        return False


newF = filter(myf, forums.values())

# Вариант 2 - с помощью row_width и insert.
choice = InlineKeyboardMarkup(row_width=2)

buy_pear = InlineKeyboardButton(text="Новые", callback_data=buy_callback.new(feed_type="0"))
choice.insert(buy_pear)

buy_apples = InlineKeyboardButton(text="Обновы", callback_data="feed:updates")
choice.insert(buy_apples)

cancel_button = InlineKeyboardButton(text="Отмена", callback_data="cancel")
choice.insert(cancel_button)


cat_choice = InlineKeyboardMarkup(row_width=1)
for el in newF:
    elem = InlineKeyboardButton(text=el['category'][-1], callback_data=f"feeds:{el['category'][0]}")
    cat_choice.insert(elem)
cat_choice.insert(buy_pear)


# А теперь клавиатуры со ссылками на товары
pear_keyboard = InlineKeyboardMarkup(inline_keyboard=[
    [
        InlineKeyboardButton(text="Купи тут", url=URL_APPLES)
    ]
])
apples_keyboard = InlineKeyboardMarkup(inline_keyboard=[
    [
        InlineKeyboardButton(text="Купи тут", url=URL_PEAR)
    ]
])
