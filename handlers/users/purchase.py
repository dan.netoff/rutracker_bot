import logging

from aiogram.dispatcher.filters import Command
from aiogram.types import Message, CallbackQuery

from keyboards.inline.callback_datas import buy_callback
from keyboards.inline.choice_buttons import choice, cat_choice, pear_keyboard, apples_keyboard
from loader import dp, bot

from config import admin_id
from trecker import get_feed
from cat import get_forums
forums = {}
forums = get_forums(forums)
nl = '\n'


@dp.message_handler(Command("feed"))
async def show_items(message: Message):
    await message.answer(text="Привет, это не официальный журнал обновлений Rutracker.org \n"
                              "в Журнале 100 записей, можно выбрать какие посмотреть",
                         reply_markup=cat_choice)


# Попробуйем отловить по встроенному фильтру, где в нашем call.data содержится "pear"
@dp.callback_query_handler(buy_callback.filter())
async def buying_pear(call: CallbackQuery, callback_data: dict):

    # logging.info(f"{callback_data['feed_type']}")

    topics = get_feed(callback_data['feed_type'])
    my_tops = list(topics.values())
    my_tops = sorted(my_tops, key=lambda i: int(i['down'].replace(',', '')))

    for topic in my_tops:
        print(topic)
        try:
            link = f'<a href="{topic["link"]}">{topic["just_title"]}</a>'
            text = (f'{forums[topic["category"]]["main"][-1]}'
            f'{nl}{forums[topic["category"]]["forum"][-1]}'
            f'{nl}{forums[topic["category"]]["category"][-1]}'
            f'{nl}{topic["id"]}, {topic["down"]}'
            f'{nl}{link}')
        except:
            text = 'Error'

        await call.message.answer(disable_web_page_preview=True, parse_mode='HTML', text=text)




# Попробуем использовать фильтр от CallbackData
@dp.callback_query_handler(buy_callback.filter(feed_type="updates"))
async def buying_apples(call: CallbackQuery, callback_data: dict):
    # Выведем callback_data и тут, чтобы сравнить с предыдущим вариантом.
    logging.info(f"{callback_data}")
    # quantity = callback_data.get("quantity")
    await call.message.answer(f"Вы выбрали купить яблоки. Яблок всего. Спасибо.",
                              reply_markup=apples_keyboard)
    # await call.answer(cache_time=60)


@dp.callback_query_handler(text="cancel")
async def cancel_buying(call: CallbackQuery):
    # Ответим в окошке с уведомлением!
    await call.answer("Вы отменили эту покупку!", show_alert=True)
    await call.message.edit_reply_markup(reply_markup=None)
