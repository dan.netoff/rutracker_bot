from bs4 import BeautifulSoup
import requests
import lxml
from datetime import datetime
import itertools
import re
# from main import feed_topics

feed_topics = []


def get_feed(category):

    url = f'http://feed.rutracker.cc/atom/f/{category}.atom'
    page = requests.get(url)
    soup = BeautifulSoup(page.text, "xml")
    soup = soup.find_all('entry')
    feed_topics = []
    topics = {}
    for i in soup:
        id = i.find('id').get_text().split('/t/')[-1]
        updated = datetime.strptime(i.find('updated').get_text()[:19], '%Y-%m-%dT%H:%M:%S')
        top_id = f'{id}+{updated}'

        if top_id not in feed_topics:
            feed_topics.append(top_id)
            topics[id] = {
                    'id': id,
                    'link': i.find('link').get('href'),
                    'updated': updated,
                    'name': i.find('name').get_text(),
                    'title': i.find('title').get_text(),
                    'category': i.find('category').get('term').split('-')[-1],
                    'just_title': (re.sub("[\(\[].*?[\)\]]", "", i.find('title').get_text()).strip())
                }
    print('feed : ' + str(len(topics)))
    # SOLO
    # r = requests.post('https://rutracker.org/forum/viewtopic.php?t=5997511', cookies=cookies)
    # soup = BeautifulSoup(r.text, "html.parser")

    topics = dict(itertools.islice(topics.items(), 100))

    topic_url = 'https://rutracker.org/forum/viewtopic.php?t='
    cookies = {'bb_session': "0-11105181-G55NtC725f5yRwcre0bS"}
    for topic in topics.values():
        print(topic['id'])
        r = requests.post(topic_url + topic['id'], cookies=cookies)
        soup = BeautifulSoup(r.text, "html.parser")
        topic_stat = soup.find_all('table', {'class': 'attach bordered med'})
        topic['down'] = ''
        topic['is_new'] = ''
        topic_ul = ''
        if len(topic_stat) > 0:
            topic_ul = topic_stat[0].find_all('ul', {'class': 'inlined middot-separated'})
            print(topic_ul[1].find_all('li'))
            topic['down'] = topic_ul[1].find_all('li')[1].string.split(' ')[1]
            topic_posted_since = len(soup.find_all('span', {'class': 'posted_since hide-for-print'})[0].string)
            topic['is_new'] = 'Новый' if topic_posted_since < 30 else ''
        topic['is_topic'] = 'Топик' if len(topic_stat) > 0 else ''

    new_topics = {k: v for k, v in topics.items() if v['is_topic']}
    print('send : ' + str(len(new_topics)))

    return new_topics


