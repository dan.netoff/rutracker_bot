from bs4 import BeautifulSoup


def get_forums(forums):
    with open("CAT.html", encoding='utf-8') as f:
        contents = f.read()
    soup = BeautifulSoup(contents, "lxml")
    main_uls = soup.find_all('ul', {'class': 'tree-root'}, recursive=True)
    for main_ul in main_uls:
        main_li = main_ul.find_all('li', recursive=False)
        title = main_li[0].find_all('span', {'class': 'c-title'})[0]
        main_name = title.get('title')
        sub_ul = main_li[0].find_all('ul', recursive=False)
        sub_lis = sub_ul[0].find_all('li', recursive=False)
        for sub_li in sub_lis:
            forum = str(sub_li.a.get('href'))
            forum_name = sub_li.a.text
            forums[forum] = {
                'category': [forum, forum_name],
                'forum': [forum, forum_name],
                'main': ['', main_name],
            }
            # print(f'{forum} {forum_name}')
            uls = sub_li.find_all('ul', recursive=False)
            if len(uls) > 0:
                lis = uls[0].find_all('li', recursive=False)
                for li in lis:
                    category = str(li.a.get('href'))
                    category_name = li.a.text
                    forums[category] = {
                        'category': [category, category_name],
                        'forum': [forum, forum_name],
                        'main': ['', main_name],
                    }
                    # print(f' -{category} {category_name}')
    return forums
